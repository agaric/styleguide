(function ($) {
$(document).ready(function(){
  // Open side navbar for handsets
  $('.navbar-burger').on('click tap', function(e) {
    $(this).toggleClass('is-active');
    $(this).parent().parent().find('#navbar-menu').toggleClass('is-active');
  });

  // Swap navbar colors with ScrollMagic.js
  var controller = new ScrollMagic.Controller();
  var sections = $('section[data-headerbg]');
  $.each(sections, function(index,item){
    var el = $(item),
      elClass = el.data('headerbg') + '-header',
      headerEl = $('#navbar');
    new ScrollMagic.Scene({
        triggerElement: el,
        triggerHook: 'onLeave',
        duration: el.outerHeight()
      })
      .offset(el.offset().top - headerEl.outerHeight())
      .on("enter",function(e){headerEl.addClass(elClass)})
      // Minimize flash of different navbar color by not having it transition in
      // the first time, but really we need a full fix not a meh mitigation.
      .on("leave",function(e){headerEl.removeClass(elClass); headerEl.addClass('navbar-transition')})
      .addTo(controller);
  });

  // Slick carousel
  $('.carousel').slick({
    arrows: false,
    centerMode: true,
    centerPadding: '0',
    slidesToShow: 3,
    dots: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.navbar-search-swap').hover(function() {
    $(this).find('input').focus();
  });

});
})(jQuery);
