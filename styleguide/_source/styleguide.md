Style Guide
===========

## Styleguide Contents

This project contains SASS/CSS and markup samples for the Agaric.com website and any associated websites, including data.agaric.com. SASS source code is compiled to CSS. Handlebars templates are compiled to HTML markup examples.

The SASS/CSS code is built on the [Bulma framework](https://bulma.io).

The markup examples are comiled by the [Assemble static site generator](http://assemble.io). _(It is not necessary to use this in your application.)_

Here is a description of the four directories:

- **assets:** compiled CSS, fonts, and images for production
- **design-output:** compiled HTML
- **design-source:** the templates for the markup samples
- **sass:** the uncompiled SASS files

Here are the complete static layouts:

* [Home](../static-layouts/design-output/index.html)
* [Work](../static-layouts/design-output/work.html)
* [Case Study](../static-layouts/design-output/case_study.html)
* [Initiatives](../static-layouts/design-output/initiatives.html)
* [Initiative](../static-layouts/design-output/initiatives_initiative.html)
* [Team](../static-layouts/design-output/about_team.html)
* [Person](../static-layouts/design-output/about_team_member.html)
* [Events](../static-layouts/design-output/about_events.html)
* [Event](../static-layouts/design-output/about_events_event.html)
* [Insights](../static-layouts/design-output/insights.html)
* [Insight](../static-layouts/design-output/insights_insight.html)
* [Bulma Elements](../static-layouts/design-output/bulma.html)

At the bottom of each static layout there are links to one another and back to this styleguide.

## Up and running

1. Install Grunt, `npm install -g grunt-cli`, and `npm install -g grunt`
2. Go to the project directory: `cd [PATH_TO_PROJECT]`
3. Install the node modules: `npm install`
4. Run the command `grunt`.


## Image Processing

Creating the duotone images can be accomplished using the [ImageMagick library](http://www.imagemagick.org).

This is a two-step process. The first step is to create a black and white (grayscale) image:

`convert test.jpg -colorspace Gray test_gray.jpg`

The second step is to apply a color lookup table:

`convert test_gray.jpg duo_cinnamon_clut.png -clut test_duo.jpg`

The following duotone color lookup table .PNG files can be found in `..\..\static-layouts\image-processing\`:

* `duo_berry_clut.png`* `duo_blue_clut.png`* `duo_briteblue_clut.png`* `duo_cinnamon_clut.png`* `duo_dandelion_clut.png`* `duo_marine_clut.png`

