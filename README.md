# About the Agaric Pattern Library & Style Guide

This living styleguide is powered by the same styles as power the Agaric site itself.  Changes in `agaric/sass/agaric/` will be reflected both in the styleguide and ultimately on the live sites, so be cautious!

## Set up

#### Use the right version of node with:
`nvm use`

_This command will look at your `.nvmrc` file and use the version node.js specified in it. This ensures all developers use the same version of node for consistency._

#### If that version of node isn't installed, install it with:
`nvm install`

#### Install npm dependencies with
`npm install`

_This command looks at `package.json` and installs all the npm dependencies specified in it.  Some of the dependencies include grunt and grunt-sass._

## Usage

### Compile CSS

`grunt sass`

See more available Grunt commands:

`grunt --help`


## Stuff we're not using

### Rebuild the style guide

`grunt kss`

### Run the static design layouts

`grunt`

_This will open the static layouts in your browser.  At the bottom you can click between them and to the style guide._

## Contents

The `agaric` folder contains the styles, as Sass .scss files, that we expect to be applicable to all Agaric-branded web applications.

### Markup examples and static layouts

See `styleguide/index.html` for project info.
